import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { testcomponent} from './testcomponent/testcomponent.component';
import { TerzocomponentComponent } from './terzocomponent/terzocomponent/terzocomponent.component'
import { FormsModule } from '@angular/forms';
import { FormComponent } from './form/form.component';
import { TableComponent } from './table/table.component';
import { BackgroundColorDirective } from './background-color.directive';
import { Table2Component } from './table2/table2.component'
@NgModule({
  declarations: [
    AppComponent,
    testcomponent,
    TerzocomponentComponent,
    FormComponent,
    TableComponent,
    BackgroundColorDirective,
    Table2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
