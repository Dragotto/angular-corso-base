import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string = 'tutorialita';
  nome: string;
  eta: number = 18;
  condizione:boolean;
  oggetto: {nome:string, eta:number }
  arrayOggetti: [
	{nome:string, eta:number}
];
qualsiasitipodinamico: any;
stampatesto: string;
testo2: string;
nomelabel: '';
persone = ['Piero','Paolo','Giovanni'];
users = [
  {nome:'Piero',citta:'Roma'},
  {nome:'Paolo',citta:'Napoli'},
  {nome:'Giovanni',citta:'Milano'}
];
valore = 1000;
booleano=true;
utenti= [
  {nome: 'piero',citta:"Roma"},
  {nome: 'mario',citta:"Napoli"},
  {nome: 'simone',citta:"Ostia"}
];

userseta = [
  {nome:'Piero',citta:'Roma',eta:33},
  {nome:'Paolo',citta:'Napoli',eta:31},
  {nome:'Giovanni',citta:'Milano',eta:32}
];

 getUser(event:{nome:string,citta:string}){
   this.utenti.push(
    {nome: event.nome,citta: event.citta}
   );
 }


constructor() {
  setTimeout (() => {
       this.nome = 'Piero';
       },2000);
      }

getClick(event){
  this.nome = "Paolo";
  console.log('Nome cambiato in Paolo!')
}      

getText(event){
  this.stampatesto = event.target.value;
}

gettesto3data(input){
  console.log(input);
}

aggiungiNome() {
this.persone.push(this.nomelabel);

}

}
