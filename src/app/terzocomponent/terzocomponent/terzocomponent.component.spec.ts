import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerzocomponentComponent } from './terzocomponent.component';

describe('TerzocomponentComponent', () => {
  let component: TerzocomponentComponent;
  let fixture: ComponentFixture<TerzocomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerzocomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerzocomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
